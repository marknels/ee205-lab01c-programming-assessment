from itertools import chain



def initialize_frequency_dict():
	frequency_dict = dict()
	
	for letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ-":
		frequency_dict[ letter ] = int(0)

	return frequency_dict
	


def add_letter_frequencies( frequency_dict, inString ):
	
	for letter in inString:
		letter = letter.upper()
		# print( letter )      # @todo:  Remove before flight - Use for debugging
		
		if( letter >= 'A' and letter <= 'Z' ):
			frequency_dict[ letter ] += 1
		else:
			frequency_dict[ '-' ] += 1



def print_letter_frequency( frequency_dict ):
	
	total_letters = int(0)
	for letter_count in frequency_dict.values():
		total_letters += letter_count
	
	
	# This lambda function reverse-sorts the dictionary into a list of letters and counts
	# Chain was imported from itertools
	final_output = list(chain(*sorted( frequency_dict.items()
	                                  ,key=lambda x: x[1]
	                                  ,reverse=True )))
	
	i = 0
	while i < len(final_output):
		letter = final_output[i]
		letter_count = final_output[i+1]
		print( letter + ": " 
		      +str(letter_count) + " " 
		      +"%.3f" % (letter_count / total_letters * 100) +"%" )
		
		i += 2








# main functionality

print("Letter frequency of the preamble to the Constitution of the United States ")

the_preamble = ( "We the People of the United States, in Order to form a more "
                 "perfect Union, establish Justice, insure domestic Tranquility, "
                 "provide for the common defence, promote the general "
                 "Welfare, and secure the Blessings of Liberty to ourselves "
                 "and our Posterity, do ordain and establish this Constitution "
                 "for the United States of America." )


frequency_dict = initialize_frequency_dict()

add_letter_frequencies( frequency_dict, the_preamble )

print_letter_frequency( frequency_dict )
